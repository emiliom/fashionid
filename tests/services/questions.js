var mocha = require("mocha");
var chai = require('chai');
var fs = require('fs');
var expect = chai.expect;

var questionService = require('./../../services/questions');

describe('QuestionService', function() {

  describe('without initialization', function() {
    it('getAnswer() with question id 1 should return undefined', function() {
      expect(questionService.getAnswer(1)).to.be.undefined;
    });
    it('getAnswers() should return an empty array', function() {
      expect(questionService.getAnswers()).to.be.empty;
    });
  });

  describe('with initialization from addressbook_1.csv (With oldest Bill/Paul)', function() {
    beforeEach(function() {
      var addressBookFixture = fs.readFileSync(__dirname+'/./../fixtures/addressbook_1.csv', 'utf8');
      questionService.initialize(addressBookFixture);
    });

    var expectedAnswer1 = {
      number: 1,
      answer: 'The number of females in the address book is 1.'
    };

    var expectedAnswer2 = {
      number: 2,
      answer: 'The oldest person in the address book is "Bill McKnight", born Tue Jan 01 1980.'
    };

    var expectedAnswer3 = {
      number: 3,
      answer: 'Bill is 366 days older than Paul.' // 1980 was a leap year
    };

    it('getAnswer() with question id 1 should return an answer object with the correct answer', function() {
      expect(questionService.getAnswer(1)).to.deep.equal(expectedAnswer1);
    });

    it('getAnswer() with question id 2 should return an answer object with the correct answer', function() {
      expect(questionService.getAnswer(2)).to.deep.equal(expectedAnswer2);
    });

    it('getAnswer() with question id 3 should return an answer object with the correct answer', function() {
      expect(questionService.getAnswer(3)).to.deep.equal(expectedAnswer3);
    });

    it('getAnswers() should return an array of all correct answers', function() {
      expect(questionService.getAnswers()).to.deep.equal([expectedAnswer1, expectedAnswer2, expectedAnswer3]);
    })
  });

  describe('with initialization from addressbook_2.csv (No Bill/Paul)', function() {
    beforeEach(function() {
      var addressBookFixture = fs.readFileSync(__dirname+'/./../fixtures/addressbook_2.csv', 'utf8');
      questionService.initialize(addressBookFixture);
    });

    var expectedAnswer1 = {
      number: 1,
      answer: 'The number of females in the address book is 3.'
    };

    var expectedAnswer2 = {
      number: 2,
      answer: 'The oldest person in the address book is "Emilio Markgraf", born Sat Apr 06 1985.' // Not "Emilio Markgraf2", because "Emilio Markgraf" comes first
    };

    var expectedAnswer3 = {
      number: 3,
      answer: 'This question cannot be answered because Bill or Paul are not in the address book.'
    };

    it('getAnswer() with question id 1 should return an answer object with the correct answer', function() {
      expect(questionService.getAnswer(1)).to.deep.equal(expectedAnswer1);
    });

    it('getAnswer() with question id 2 should return an answer object with the correct answer', function() {
      expect(questionService.getAnswer(2)).to.deep.equal(expectedAnswer2);
    });

    it('getAnswer() with question id 3 should return an answer object with the correct answer', function() {
      expect(questionService.getAnswer(3)).to.deep.equal(expectedAnswer3);
    });

    it('getAnswers() should return an array of all correct answers', function() {
      expect(questionService.getAnswers()).to.deep.equal([expectedAnswer1, expectedAnswer2, expectedAnswer3]);
    })
  });
});
