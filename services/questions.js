var parse = require('csv-parse/lib/sync');

var questionService = function() {};

/**
 * Question storage array.
 * @type {Array}
 */
questionService.questions = [];

/**
 * Address book data storage array.
 * @type {Array}
 */
questionService.data = [];

/**
 * Initialize data store with address book data, initialize questions.
 * Questions are stored as callbacks in the questions array inside the question service.
 *
 * @param addressBook
 */
questionService.initialize = function(addressBook) {
  this.questions = [];
  this.data = [];

  this.processAddressBook(addressBook);

  // How many women are in the address book?
  var question1 = function(data) {
    var females = data.filter(function(person) {
      return 'female' == person.gender.toLowerCase();
    });

    return 'The number of females in the address book is '+females.length+'.';
  };

  // Who is the oldest person in the address book?
  var question2 = function(data) {
    var oldestPerson = undefined;

    data.forEach(function(person) {
      if (!oldestPerson) {
        oldestPerson = person;
      }

      // What happens if both birthdays are equal is not defined. So first come, first serve.
      if (person.birthDay < oldestPerson.birthDay) {
        oldestPerson = person;
      }
    });

    return 'The oldest person in the address book is "'+oldestPerson.name+'", born '+oldestPerson.birthDay.toDateString()+'.';
  };

  // How many days older is Bill than Paul?
  var question3 = function(data) {
    var bill = undefined;
    var paul = undefined;

    // who is bill? who is paul? Find them in the address book. Since we only have their first names, it's first come first serve again.
    for (var i = 0; i < data.length; ++i) {
      var person = data[i];

      if (undefined == bill && 'bill' == person.name.split(' ')[0].toLowerCase()) {
        bill = person;
      }
      if (undefined == paul && 'paul' == person.name.split(' ')[0].toLowerCase()) {
        paul = person;
      }

      if (bill && paul) {
        break;
      }
    }

    if (undefined === bill || undefined == paul) {
      return 'This question cannot be answered because Bill or Paul are not in the address book.';
    }

    // The question is how many days older is bill than paul, which means we assume Bill is older than Paul.
    // Nevertheless we subtract paul from bill, because `Date().getTime()` gives us the number of milliseconds
    // between that date and midnight of 01.01.1970. This means, that even if we work under the assumption
    // that Bill is older, Paul will have the bigger number of elapsed milliseconds. And since we want a positive answer
    // if our assumption is correct, we subtract bill from paul (or multiply the result with -1).
    // Math.abs() is deliberately not used here because if the assumption turns out to be wrong, the result should be
    // negative to reflect that.
    var timeDiff = paul.birthDay.getTime() - bill.birthDay.getTime();
    var diffDays = Math.ceil(timeDiff / 86400000); // 86400000 = 1000 (milliseconds in a second) * 60 (seconds in a minute) * 60 (minutes in an hour) * 24 (hours in a day);

    return 'Bill is '+diffDays+' days older than Paul.';
  };

  // Each entry in the questions array is an object consisting of the number of the question and a callback function
  // that returns a string with the answer to that question.
  // The number used in this object is later used for dynamic routing, e.g. add a question with the number 123, and
  // the route questions/123 will automatically work.
  this.questions.push({'number': 1, 'cb': question1});
  this.questions.push({'number': 2, 'cb': question2});
  this.questions.push({'number': 3, 'cb': question3});
};

/**
 * This method is part of the initialization process.
 * It takes the raw data of an address book, parses it as CSV and stores the information for later use.
 *
 * @param addressBook
 */
questionService.processAddressBook = function(addressBook) {
  var parsedData = parse(addressBook, {trim: true, columns: ['name', 'gender', 'birthDay']});

  // Reformat birthday string and convert to date object
  this.data = parsedData.map(function(row) {
    var tmpBirthday = row.birthDay.split('/');
    tmpBirthday[2] = '19'+tmpBirthday[2];
    row.birthDay = new Date(tmpBirthday.reverse().join('-'));

    return row;
  });
};

/**
 * Returns an answer object for the requested question.
 * Returns undefined if the question cannot be found.
 *
 * @param number number of the question
 *
 * @returns {Object|undefined}
 */
questionService.getAnswer = function(number) {
  var self = this;
  var answer = undefined;

  this.questions.forEach(function(question) {
    if (number == question.number) {
      answer = {
        number: question.number,
        answer: question.cb(self.data)
      };
    }
  });

  return answer;
};

/**
 * Answers all questions known to the system.
 * @see getAnswer
 *
 * @returns {Array} Array of answer objects, one for each question.
 */
questionService.getAnswers = function() {
  var self = this;
  var result = [];

  this.questions.forEach(function(question) {
    result.push(self.getAnswer(question.number));
  });

  return result;
};

module.exports = questionService;
