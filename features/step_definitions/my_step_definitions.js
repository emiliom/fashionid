module.exports = function () {
  this.Given(/^I am on the '(.*)' page$/, function(page, callback) {
    this.visit('http://localhost:3000'+page, callback);
  });

  this.Then(/^I should see the content$/, function (content, callback) {
    var bodyText = this.browser.html('body').replace('<body>', '').replace('</body>', '');//.trim().replace(/<br>/g, "\n");
    content = content.replace(/\n/g, '<br>')+"<br>";

    if (content == bodyText) {
      callback();
    } else {
      callback('Response: "'+bodyText+'", Expected: "'+content+'"');
    }
  });
};
