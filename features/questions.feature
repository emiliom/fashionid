Feature: Questions page
  As a user of this question software
  I want to see the answers to all questions on one page

  Scenario: Visiting questions page
    Given I am on the '/questions' page
    Then I should see the content
      """
1. The number of females in the address book is 2.
2. The oldest person in the address book is "Wes Jackson", born Wed Aug 14 1974.
3. Bill is 2862 days older than Paul.
      """
