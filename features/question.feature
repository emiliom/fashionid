Feature: Single Question
  As a user of this question software
  I want to be able to see one answer at the time

  Scenario: Visiting the first question
    Given I am on the '/questions/1' page
    Then I should see the content
      """
1. The number of females in the address book is 2.
      """

  Scenario: Visiting the second question
    Given I am on the '/questions/2' page
    Then I should see the content
      """
2. The oldest person in the address book is "Wes Jackson", born Wed Aug 14 1974.
      """

  Scenario: Visiting the third question
    Given I am on the '/questions/3' page
    Then I should see the content
      """
3. Bill is 2862 days older than Paul.
      """
