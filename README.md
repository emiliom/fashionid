# FashionId Hausaufgabe

## Installation

Tools needed to run this software:

- grunt (tested with v1.0.1 and grunt-cli v1.2.0)
- node (tested with v6.4.0)
- npm (tested with 3.10.3)

To install, run a `git clone git@bitbucket.org:emiliom/fashionid.git` followed by a `cd fashionid` and a `npm install`.

## Running

To run this software (after installation), go to the root folder of the project and run `grunt` command. Afterwards, you can point your browser to the following urls:

- [http://localhost:3000/questions](http://localhost:3000/questions)
- [http://localhost:3000/questions/1](http://localhost:3000/questions/1)
- [http://localhost:3000/questions/2](http://localhost:3000/questions/2)
- [http://localhost:3000/questions/3](http://localhost:3000/questions/3)

## Testing

### Unit tests

To run the unit tests, go to the root folder of the project and run `node_modules/.bin/mocha tests --recursive`
The sources for these tests can be found in the `./tests/` folder.

### Acceptance tests

To run the acceptance tests, you need two open terminals.

In the first terminal, go to the root folder of the project and run `grunt` to start and run the development server that listens to `localhost:3000`. This is necessary because the acceptance tests are frontend tests that emulate a browser and therefore need a HTTP-Server to connect to.

In the second terminal, go to the root folder of the project and run `node_modules/.bin/cucumber.js`.
The sources for these tests can be found in the `./features/` folder.


##### Emilio Markgraf <emilio.markgraf@gmail.com>
