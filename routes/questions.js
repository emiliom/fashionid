var questionService = require('../services/questions');
var express = require('express');
var router = express.Router();

// Route for all answers.
router.get('/', function(req, res, next) {
  var answers = questionService.getAnswers();

  res.render('questions', {answers: answers});
});

// Route for a single answer.
// If the requested question cannot be found, a 404 response is returned.
router.get('/:questionId', function(req, res, next) {
  var questionId = req.params.questionId;
  var answer = questionService.getAnswer(questionId);

  if (!answer) {
    var err = new Error('Unknown question');
    err.status = 404;
    next(err);
    return;
  }

  res.render('question', {
    answer: answer
  });
});

module.exports = router;
